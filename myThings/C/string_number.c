#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @breif - Tranforms a string into a decimal number assuming each letter has a direct corresponce to a 32 bit  number, since there are only 26 letter in the alphabet it is doable
 * this way
 * Every letter only counts one time no matter how many times it appaers in the string 
 */
void string_number(char *string)
{
    long long int number = 0;

    for (int i = 0; i < strlen(string); i++)
        for (int j = i + 1; j < strlen(string); j++)
            if (string[j] == string[i])
                string[j] = '0';

    for (int i = 0; i < strlen(string); i++)
        if (string[i] != 0)
            number += pow(2, string[i] - 'a');

    printf("%lld \n", number);

    return;
}

int main()
{
    char *string = calloc(26, sizeof(char));

    do
    {
        printf("Escreva a string: \n");
        scanf("%s", string);
        string_number(string);
    } while (strcmp(string, "exit") != 0);

    return 0;
}