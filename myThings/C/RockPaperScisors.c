/**
 * @Program Descrition -> Pedra Papel Tesoura bastante simples com 3  modos possiveis, C-C / C-P/ P-P
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_STRING_LENGTH 1000

void introMessage();
void rulesMessage();
int decodePlayerPlay(char *playerPlay);
int checkVictory(int player1Play, int player2play);
void decodeKonsoleInput(char *argv[], int argc, int *Konsole);
void gameMode0(int *player1Victory, int *player2Victory, int *rounds);
void gameMode1(int *player1Victory, int *player2Victory, int *rounds);
void gameMode2(int *player1Victory, int *player2Victory, int *rounds);
void printResults(int player1Victory, int player2Victory);
char *getPlayFromNumber(int playerPlay);

/**
 * @brief Main function that controls the program
 * @param argc - number of arguments
 * @param argv - vector that holds Konsole Input
 */
int main(int argc, char *argv[])
{
    int player1Victory = 0;
    int player2Victory = 0;
    int KonsoleInputDecod[2] = {0};

    decodeKonsoleInput(argv, argc, KonsoleInputDecod);

    srand(time(0));

    introMessage();

    rulesMessage();

    if (KonsoleInputDecod[1] == 0)
        gameMode0(&player1Victory, &player2Victory, &KonsoleInputDecod[0]);
    else if (KonsoleInputDecod[1] == 1)
        gameMode1(&player1Victory, &player2Victory, &KonsoleInputDecod[0]);
    else if (KonsoleInputDecod[1] == 2)
        gameMode2(&player1Victory, &player2Victory, &KonsoleInputDecod[0]);
    else
    {
        printf("Invalid Konsole Input\n");
        exit(EXIT_FAILURE);
    }

    printResults(player1Victory, player2Victory);
}

/**
 * @brief From a given string this function will decode the correspondent number of the play  
 * @param playerPlay - Pointer to the string that saves the player play
 * @return Integer with the correspondent play
 */
int decodePlayerPlay(char *playerPlay)
{
    if (!strcmp(playerPlay, "Pedra"))
        return 0;
    else if (!strcmp(playerPlay, "Papel"))
        return 1;
    else if (!strcmp(playerPlay, "Tesoura"))
        return 2;
    return -1; // Error
}

/**
 * @brief Check if any of the player as won this round
 * @param player1Play - The integer number that is associated with the first player play
 * @param player2Play - The integer number that is associated with the second player play
 * @return Interger number that is used to read if there was a win, and if so the player that won
 */
int checkVictory(int player1Play, int player2play)
{
    if (player1Play == player2play)
        return 0;
    if (player1Play == 0) // player 1 = Pedra
    {
        if (player2play == 1) // player 2 = Papel
            return 2;
        if (player2play == 2) // player 2 = Tesoura
            return 1;
    }
    if (player1Play == 1) // player 1 = Papel
    {
        if (player2play == 0) // player 2 = Pedra
            return 1;
        if (player2play == 2) // player 2 = Tesoura
            return 2;
    }
    if (player1Play == 2) // player 1 = Tesoura
    {
        if (player2play == 0) // player 2 = Pedra
            return 2;
        if (player2play == 1) // player 2 = Papel
            return 1;
    }
    return -1;
}

/**
 * @brief Return the string that is associated with the interger of the player play 
 * @param playerPlay - The integer number associated with a sepecific player play
 * @return Pointer to a string that contains the extense name of the player play
 */
char *getPlayFromNumber(int playerPlay)
{
    if (playerPlay == 0)
        return "Pedra";
    else if (playerPlay == 1)
        return "Papel";
    else if (playerPlay == 2)
        return "Tesoura";
    else
        return "Jogada desconhecida";
}

/**
 * @brief Prints the rules of the game 
 */
void rulesMessage()
{
    printf("O jogo é bastante simples, tem 3 jogadas possiveis 'Pedra', 'Papel' e 'Tesoura'\n");
    printf("'Pedra' => ganha a 'Tesoura' e perder com 'Papel'\n");
    printf("'Papel' => ganha a 'Pedra' e perde com 'Tesoura' \n");
    printf("'Tesoura' => ganha a 'Papel' e perde com 'Pedra' \n\n\n");
}

/**
 * @brief Print the beatiful game logo with the information of when it was created
 */
void introMessage()
{
    printf("*********************************************************************************\n");
    printf("*                                 WELCOME TO :                                  *\n");
    printf("*                             Pedra Papel Tesouras                              *\n");
    printf("*                  game done by: André Teixeira - 02/09/2021                    *\n");
    printf("*********************************************************************************\n");
}

/**
 * @brief Controls the game mode of player vs player 
 * @param player1Victory - passes a pointer to the integer created on main that will hold the player1 ammount of victors 
 * @param player2Victory - passes a pointer to the integer created on main that will hold the player2 ammount of victors
 * @param rounds - passes a pointer to the interger created on main that will hold the ammount of rounds the full game should have
 */
void gameMode2(int *player1Victory, int *player2Victory, int *rounds)
{
    int player1Play = 0;
    int player2Play = 0;
    char player2[MAX_STRING_LENGTH];
    char player1[MAX_STRING_LENGTH];
    int gameResult = 0;

    do
    {

        printf("Player 1 time to play\n");

        if (scanf("%s", player1) != 1)
        {
            printf("Erro ao ler o valor \n");
            exit(EXIT_FAILURE);
        }

        printf("Player 2 time to play\n");
        if (scanf("%s", player2) != 1)
        {
            printf("Erro ao ler o valor\n");
            exit(EXIT_FAILURE);
        }

        player1Play = decodePlayerPlay(player1);
        player2Play = decodePlayerPlay(player2);

        if (player2Play == -1 || player1Play == -1)
        {
            printf("jogada inválida \n");
            exit(EXIT_FAILURE);
        }

        gameResult = checkVictory(player1Play, player2Play);

        if (gameResult == 1)
        {
            *rounds -= 1;
            *player1Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == 2)
        {
            *rounds -= 1;
            *player2Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == -1)
        {
            printf("An error occured while playing the game \n");
            exit(EXIT_FAILURE);
        }
    } while (*rounds != 0);
}

/**
 * @brief Controls the game mode of player vs computer 
 * @param player1Victory - passes a pointer to the integer created on main that will hold the player1 ammount of victors 
 * @param player2Victory - passes a pointer to the integer created on main that will hold the player2 ammount of victors
 * @param rounds - passes a pointer to the interger created on main that will hold the ammount of rounds the full game should have
 */
void gameMode1(int *player1Victory, int *player2Victory, int *rounds)
{
    int player1Play = 0;
    int player2Play = 0;
    char player2[MAX_STRING_LENGTH];
    int gameResult = 0;

    do
    {

        player1Play = rand() % 3;

        printf("Player 2 time to play\n");

        if (scanf("%s", player2) != 1)
        {
            printf("Erro ao ler o valor \n");
            exit(EXIT_FAILURE);
        }

        player2Play = decodePlayerPlay(player2);

        if (player2Play == -1)
        {
            printf("jogada inválida \n");
            exit(EXIT_FAILURE);
        }

        gameResult = checkVictory(player1Play, player2Play);

        if (gameResult == 1)
        {
            *rounds -= 1;
            *player1Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == 2)
        {
            *rounds -= 1;
            *player2Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == -1)
        {
            printf("An error occured while playing the game \n");
            exit(EXIT_FAILURE);
        }
    } while (*rounds != 0);
}

/**
 * @brief Controls the game mode of computer vs computer 
 * @param player1Victory - passes a pointer to the integer created on main that will hold the player1 ammount of victors 
 * @param player2Victory - passes a pointer to the integer created on main that will hold the player2 ammount of victors
 * @param rounds - passes a pointer to the interger created on main that will hold the ammount of rounds the full game should have
 */
void gameMode0(int *player1Victory, int *player2Victory, int *rounds)
{
    int player1Play = 0;
    int player2Play = 0;
    int gameResult = 0;

    do
    {
        player1Play = rand() % 3;
        player2Play = rand() % 3;

        printf("%d - %d \n", player1Play, player2Play);

        gameResult = checkVictory(player1Play, player2Play);

        if (gameResult == 1)
        {
            *rounds -= 1;
            *player1Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == 2)
        {
            *rounds -= 1;
            *player2Victory += 1;
            printf("J1:%s J2:%s, Resultado ate agora: %d - %d\n", getPlayFromNumber(player1Play), getPlayFromNumber(player2Play), *player1Victory, *player2Victory);
        }
        else if (gameResult == -1)
        {
            printf("Ocorreu um erro a jogar o jogo \n");
            exit(EXIT_FAILURE);
        }
    } while (*rounds != 0);
}

/**
 * @brief Prints the end game results
 * @param player1Victory - passes a pointer to the integer created on main that will hold the player1 ammount of victors 
 * @param player2Victory - passes a pointer to the integer created on main that will hold the player2 ammount of victors
 */
void printResults(int player1Victory, int player2Victory)
{
    if (player1Victory == player2Victory)
        printf("EMPATE: %d-%d\n", player1Victory, player2Victory);
    else if (player1Victory > player2Victory)
        printf("Vitoria Jogador 1: %d-%d\n", player1Victory, player2Victory);
    else if (player1Victory < player2Victory)
        printf("Vitoria Jogador 2: %d-%d\n", player1Victory, player2Victory);
}

/**
 * @brief decodes the words that the player inputed in the konsole 
 * @param argv - the vector that contains all that was inputed in the konsole
 * @param argc - the ammount of arguments inputed in the konsole
 * @param konsole - Vector that will hold the decoded information
 */
void decodeKonsoleInput(char *argv[], int argc, int *Konsole)
{
    if (argc == 1)
    {
        printf("Não introduziu argumentos de  Konsola => programa não sabe o que fazer \n");
        exit(EXIT_FAILURE);
    }

    for (int i = 1; i < argc; i++)
    {
        sscanf(argv[i], "%d", &(*(Konsole + i - 1)));
    }
}
