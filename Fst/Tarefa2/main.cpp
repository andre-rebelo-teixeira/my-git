#include "main.hpp"

#define MAX_INPUT 20

void printStartMessage()
{
    cout << "Hello and welcome to the program. This program allows you to store the information of diferern veichles with diferent properties" << endl
         << "They will be stored in two diferent linked list the motorcycle one and the cars " << endl;
}

void functionCaller(char *Input, CarDealer *Dealership, Car *CarHead, Motorcycle *MotoHead)
{

    if (strcmp(Input, "add") == 0)
    {
        Dealership->add_vehicle();
    }
    else if (strcmp (Input, "edit ") == 0)
    {
        Dealership->edit_vehicle();
    }
    else if (strcmp(Input, "print") == 0)
    {
        Dealership->print_vehicles();
    }
    else if (strcmp(Input, "delet") == 0)
    {
        Dealership->remove_vehicle();
    }
}

int main()
{
    CarDealer *Dealership = new (CarDealer);
    Car *CarHead = NULL; 
    Motorcycle *MotoHead = NULL; 

    char Input[MAX_INPUT];
    Input [0] = '\0';

    printStartMessage();

    cout << "The commands you can use are the following: " << endl
        << "'add': add a car " << endl
        << "'edit': edit's one car"
        << "'print': prints all the information already input for the program" << endl
        << "'delet': delet all you already input" << endl
        << "'quit': end the program" << endl << endl;

    while (strcmp(Input, "quit") != 0)
    {
        Input[0] = '\0';

        cin >> Input;
        functionCaller(Input, Dealership, CarHead, MotoHead);
    }
    cout << "Ending the program" << endl;

    Dealership->delete_all_vehicles();
    delete (Dealership);

    return 0;
}