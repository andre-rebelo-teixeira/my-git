    #include <cars.hpp>

#define MAX 50

#include <iostream>
#include <list>
#include <QString>
#include <stdlib.h>
using namespace std;


CarDealer::CarDealer(QTableWidget *table1)
{
    this->table = table1;
    this->table->setColumnCount(5);
    QList <QString> columns;
    columns.append("Serial"); // posição - 0
    columns.append("Type"); // posição - 1
    columns.append("Number of wheels"); // posição - 2
    columns.append("Brand"); // posição  - 3
    columns.append("price"); // posição - 4

    this->table->setHorizontalHeaderLabels(columns);
    this->table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

// Coloca carros
void CarDealer::add_car(Car Carro)
{
    int line;

    table->insertRow(this->table->rowCount());
    line = table->rowCount() - 1; // -1 serve como offset (mesmo que nos arrays)

    this->table->setItem (line, 0, new QTableWidgetItem ( QString::number(Carro.get_serial())));
    this->table->setItem (line, 1, new QTableWidgetItem ("carro"));
    this->table->setItem (line, 2, new QTableWidgetItem (QString::number(Carro.get_wheels_count())));
    this->table->setItem (line, 3, new QTableWidgetItem (Carro.get_brand())); // não precisa do 'Qstring::(type)' pq é uma string acho eu
    this->table->setItem (line, 4, new QTableWidgetItem (QString::number(Carro.get_price())));
}


// Coloca motos
void CarDealer::add_moto(Motorcycle Moto)
{
    int line;

    table->insertRow(this->table->rowCount());
    line = table->rowCount() - 1; // -1 serve como offset (mesmo que nos arrays)

    this->table->setItem (line, 0, new QTableWidgetItem ( QString::number(Moto.get_serial())));
    this->table->setItem (line, 1, new QTableWidgetItem ("Moto"));
    this->table->setItem (line, 2, new QTableWidgetItem (QString::number(Moto.get_wheels_count())));
    this->table->setItem (line, 3, new QTableWidgetItem (Moto.get_brand())); // não precisa do 'Qstring::(type)' pq é uma string acho eu
    this->table->setItem (line, 4, new QTableWidgetItem (QString::number(Moto.get_price())));
}

void CarDealer::edit_car(int row, Car novo){

    // substitui marca
    this->table->removeCellWidget(row,3);
    this->table->setItem(row,3,new QTableWidgetItem (novo.get_brand() ) );

    // substitui preço
    this->table->removeCellWidget(row,4);
    this->table->setItem(row,4,new QTableWidgetItem (QString::number (novo.get_price() ) ) );
}

void CarDealer::edit_moto(int row, Motorcycle novo){

    // Rodas não são necessário substituir

    // substitui marca
    this->table->removeCellWidget(row,3);
    this->table->setItem(row,3,new QTableWidgetItem (novo.get_brand() ) );

    // substitui preço
    this->table->removeCellWidget(row,4);
    this->table->setItem(row,4,new QTableWidgetItem (QString::number (novo.get_price() ) ) );
}

// Apaga linhas completas sem substituir
void CarDealer::remove_car(int row)
{
    this->table->removeRow(row);
}

void CarDealer::remove_moto(int row)
{
    this->table->removeRow(row);
}

CarDealer::~CarDealer()
{
    this->table->clear();
}

QTableWidgetItem* CarDealer::get(int row, int column)
{
    QTableWidgetItem* item = this->table->item(row, column);
    return item;

}
