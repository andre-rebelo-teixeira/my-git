#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cars.hpp>
#include <QWidget>
#include <QString>
#include <QList>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QLineEdit>
#include <QDebug>
#include <QDesktopServices>
#include <QUrl>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    novo = new CarDealer(ui->MAINWINDOW->findChild<QTableWidget*>("tableWidget"));

    this->setWindowTitle("Garage");
    this->ui->stackedWidget->setCurrentIndex(1);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete novo;
}

// save
void MainWindow::on_pushButton_clicked(){
    QString Brand, type;
    double price, serial;

    Car Car1;
    Motorcycle Moto1;

    QWidget * widget1 = this->ui->centralwidget->findChild<QWidget*>("ADDWINDOW_2");

    QComboBox * combo1 = widget1->findChild<QComboBox*>("comboBox");
    type = combo1->itemText(combo1->currentIndex());

    QDoubleSpinBox * spin = widget1->findChild<QDoubleSpinBox*> ("doubleSpinBox");
    price = spin->value();

    QSpinBox * spin1 = widget1->findChild<QSpinBox*> ("spinBox");
    serial = spin1->value();

    QLineEdit * line = widget1->findChild<QLineEdit*> ("lineEdit");
    Brand = line ->displayText();

    if (type == "Car")
    {
        Car1.set_serial(serial);
        Car1.set_brand(Brand);
        Car1.set_price(price);
        Car1.set_Wheels(4); // vão ser sempre as mesmas desta vez

        this->novo->add_car(Car1);
    }
    else if(type == "Moto")
    {
        Moto1.set_serial(serial);
        Moto1.set_brand(Brand);
        Moto1.set_price(price);
        Moto1.set_Wheels(2);

        this->novo->add_moto(Moto1);
    }
    this->ui->stackedWidget->setCurrentIndex(1);
}

// Add new
void MainWindow::on_pushButton_2_clicked()
{
    QWidget * widget1 = this->ui->centralwidget->findChild<QWidget*> ("ADDWINDOW_2");
    //QLayout * Layout1 = this->ui->centralwidget->findChild<QLayout*> ("teste");

    QComboBox * combo = widget1->findChild<QComboBox*>("comboBox");
    combo->setCurrentIndex(0);

    QDoubleSpinBox * spin1 = widget1->findChild<QDoubleSpinBox*> ("doubleSpinBox");
    spin1->setMaximum(1000000000000000000);
    spin1->setValue(0.000);

    QLineEdit * line1 = widget1->findChild<QLineEdit*> ("lineEdit");
    line1->setText("");

    QSpinBox * spin2 = widget1->findChild<QSpinBox*> ("spinBox");
    spin2->setMaximum(1000000000);
    spin2->setValue(0);

    this->ui->stackedWidget->setCurrentIndex(0);
}

// save changes
void MainWindow::on_pushButton_4_clicked()
{
    double price;
    QString brand, type;
    Car Car1;
    Motorcycle Moto1;

    type = this->novo->get(row, 1)->text();

    QWidget * widget1 = this->ui->centralwidget->findChild<QWidget*> ("EDITWINDOW");

    QDoubleSpinBox * spin1 = widget1->findChild<QDoubleSpinBox*> ("doubleSpinBox_2");
    spin1->setMaximum(1000000);
    price = spin1->value();

    QLineEdit * line = widget1->findChild<QLineEdit*> ("lineEdit_2");
    brand = line->displayText();



    if (type == "carro")
    {
        Car1.set_brand(brand);
        Car1.set_price(price);
        Car1.set_Wheels(4); // vão ser sempre as mesmas desta vez

        this->novo->edit_car(row, Car1);
    }
    else if(type == "Moto")
    {
        Moto1.set_brand(brand);
        Moto1.set_price(price);
        Moto1.set_Wheels(2);

        this->novo->edit_moto(row,Moto1);
    }
    this->ui->stackedWidget->setCurrentIndex(1);
}

// delet
void MainWindow::on_pushButton_3_clicked()
{
    this->novo->remove_car(this->row);
    this->ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_tableWidget_cellClicked(int row)
{
    QString Brand;
    double price;

    price = this->novo->get(row,4)->text().toDouble();
    Brand = this->novo->get(row, 3)->text();

    QWidget * widget1 = this->ui->centralwidget->findChild<QWidget*> ("EDITWINDOW");

    QDoubleSpinBox * spin = widget1->findChild<QDoubleSpinBox*>("doubleSpinBox_2");
    spin->setValue(price);

    QLineEdit * line1 = widget1->findChild<QLineEdit*>("lineEdit_2");
    line1->setText(Brand);
    this->ui->stackedWidget->setCurrentIndex(2);
    this->row = row;
}

void MainWindow::on_pushButton_5_clicked()
{
    QString link = "https://www.google.com/search?client=opera&hs=xje&q=best+memes+ever&tbm=isch&chips=q:best+memes+ever,online_chips:memes+2020:yhnVeFjaFbs%3D&usg=AI4_-kRkR4jaWsBH1krbjFfWoz8xz6gFUw&sa=X&ved=2ahUKEwi71b__25nyAhXc8OAKHfuuAs8QgIoDKAB6BAgJEAg&biw=1880&bih=939";
    QDesktopServices:: openUrl(QUrl(link));
}
