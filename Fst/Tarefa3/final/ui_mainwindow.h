/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QStackedWidget *stackedWidget;
    QWidget *ADDWINDOW_2;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout_9;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QLabel *label;
    QLabel *label_1;
    QSpinBox *spinBox;
    QWidget *MAINWINDOW;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_10;
    QLabel *label_5;
    QPushButton *pushButton_2;
    QTableWidget *tableWidget;
    QPushButton *pushButton_5;
    QWidget *EDITWINDOW;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_8;
    QLineEdit *lineEdit_2;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBox_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setMinimumSize(QSize(800, 0));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        ADDWINDOW_2 = new QWidget();
        ADDWINDOW_2->setObjectName(QString::fromUtf8("ADDWINDOW_2"));
        verticalLayout_2 = new QVBoxLayout(ADDWINDOW_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gridLayout_9 = new QGridLayout();
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        doubleSpinBox = new QDoubleSpinBox(ADDWINDOW_2);
        doubleSpinBox->setObjectName(QString::fromUtf8("doubleSpinBox"));

        gridLayout_9->addWidget(doubleSpinBox, 1, 0, 1, 1);

        label_2 = new QLabel(ADDWINDOW_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_9->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(ADDWINDOW_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_9->addWidget(label_3, 6, 0, 1, 1);

        lineEdit = new QLineEdit(ADDWINDOW_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout_9->addWidget(lineEdit, 5, 0, 1, 1);

        comboBox = new QComboBox(ADDWINDOW_2);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_9->addWidget(comboBox, 7, 0, 1, 1);

        pushButton = new QPushButton(ADDWINDOW_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout_9->addWidget(pushButton, 8, 0, 1, 1);

        label = new QLabel(ADDWINDOW_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_9->addWidget(label, 4, 0, 1, 1);

        label_1 = new QLabel(ADDWINDOW_2);
        label_1->setObjectName(QString::fromUtf8("label_1"));

        gridLayout_9->addWidget(label_1, 2, 0, 1, 1);

        spinBox = new QSpinBox(ADDWINDOW_2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        gridLayout_9->addWidget(spinBox, 3, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_9);

        stackedWidget->addWidget(ADDWINDOW_2);
        MAINWINDOW = new QWidget();
        MAINWINDOW->setObjectName(QString::fromUtf8("MAINWINDOW"));
        verticalLayout_3 = new QVBoxLayout(MAINWINDOW);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        label_5 = new QLabel(MAINWINDOW);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_10->addWidget(label_5, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(MAINWINDOW);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        gridLayout_10->addWidget(pushButton_2, 0, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout_10);

        tableWidget = new QTableWidget(MAINWINDOW);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout_3->addWidget(tableWidget);

        pushButton_5 = new QPushButton(MAINWINDOW);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        verticalLayout_3->addWidget(pushButton_5);

        stackedWidget->addWidget(MAINWINDOW);
        EDITWINDOW = new QWidget();
        EDITWINDOW->setObjectName(QString::fromUtf8("EDITWINDOW"));
        verticalLayout_4 = new QVBoxLayout(EDITWINDOW);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_8 = new QLabel(EDITWINDOW);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_5->addWidget(label_8);

        lineEdit_2 = new QLineEdit(EDITWINDOW);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        verticalLayout_5->addWidget(lineEdit_2);


        verticalLayout_4->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_7 = new QLabel(EDITWINDOW);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_6->addWidget(label_7);

        doubleSpinBox_2 = new QDoubleSpinBox(EDITWINDOW);
        doubleSpinBox_2->setObjectName(QString::fromUtf8("doubleSpinBox_2"));

        verticalLayout_6->addWidget(doubleSpinBox_2);


        verticalLayout_4->addLayout(verticalLayout_6);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_3 = new QPushButton(EDITWINDOW);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(EDITWINDOW);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);


        verticalLayout_4->addLayout(horizontalLayout);

        stackedWidget->addWidget(EDITWINDOW);

        gridLayout->addWidget(stackedWidget, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 23));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Price - \303\251 inteiro", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "type", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("MainWindow", "Moto", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("MainWindow", "Car", nullptr));

        pushButton->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Brand", nullptr));
        label_1->setText(QCoreApplication::translate("MainWindow", "Serial - \303\251 inteiro", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Garage", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "New", nullptr));
        pushButton_5->setText(QCoreApplication::translate("MainWindow", "SURPRISE", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Brand", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "Price", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "Delet", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
